# deleting NS will delete deployed charts
kubectl delete ns microservices-demo
# easier than running 11 uninstall cmds: "helm uninstall frontend" (release name)