# create new namespace for microservices and Redis in-memory DB
# do NOT switch to this NS!
kubectl create ns microservices-demo

# deploy Redis w/ custom values files and name the release
helm install -f redis-values/redis-values.yaml redis-cart helm-charts/redis -n microservices-demo

# deploy each MS w/ custom values files and name the release
helm install -f microservice-values/email-values.yaml emailservice helm-charts/microservice -n microservices-demo

helm install -f microservice-values/productcatalog-values.yaml productcatalogservice helm-charts/microservice -n microservices-demo

helm install -f microservice-values/recommendation-values.yaml recommendationservice helm-charts/microservice -n microservices-demo

helm install -f microservice-values/shipping-values.yaml shippingservice helm-charts/microservice -n microservices-demo

helm install -f microservice-values/payment-values.yaml paymentservice helm-charts/microservice -n microservices-demo

helm install -f microservice-values/currency-values.yaml currencyservice helm-charts/microservice -n microservices-demo

helm install -f microservice-values/cart-values.yaml cartservice helm-charts/microservice -n microservices-demo

helm install -f microservice-values/ad-values.yaml adservice helm-charts/microservice -n microservices-demo

helm install -f microservice-values/checkout-values.yaml checkoutservice helm-charts/microservice -n microservices-demo

helm install -f microservice-values/frontend-values.yaml frontend helm-charts/microservice -n microservices-demo

# check statuses of all deployed components
kubectl get all -n microservices-demo